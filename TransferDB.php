<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;
use DB;
use Carbon\Carbon;
use Log;
//custom models
use App\ModelUpdateMember\Address;
use App\ModelUpdateMember\Email;
use App\ModelUpdateMember\Member;
use App\ModelUpdateMember\Name;
use App\ModelUpdateMember\Phone;

class UpdateMemberFromTxtQueueWorker implements ShouldQueue

{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $fileName;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public

    function __construct($fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try
        {
            Log::info('Start migrate data from txt file to DB');
            $fileName = $this->fileName;
            $matrix = $this->getDataFromTxtFile($fileName);
            $this->updateMembersInDb($matrix);
            Log::info('End migrate data from txt file to DB');
        }

        catch(Exception $e)
        {
            echo 'Error!';
        }
    }

    public function getDataFromTxtFile($fileName)
   {
        if (Storage::disk('local')->exists('custom-migration/' . $fileName . '.txt'))
        {
            $file1 = Storage::get('custom-migration/' . $fileName . '.txt');
  $data_input = preg_split("/(<br \/>)?(\r\n?|\n)/", nl2br($file1));
        //remove empty strings
        $data_input_chunked = collect($data_input)->chunk(100);

$matrix = $data_input_chunked
            ->map(function($collection) {
                $collection_changed = $collection
                    ->map(function($row) {
                        $row = explode("\n", $row);
                        $row = array_filter($row);
                        $row = preg_grep('/^\s*\z/', $row, PREG_GREP_INVERT);
                        $row = explode("\t", $row[0]);
                        // dd($row);
                        return $row;
                    });

                return $collection_changed;
            });
            return $matrix;
        }
        else
        {
            throw new Exception("File not found! ");
        }

    }

    public function updateMembersInDb($matrix)
    {
    try{
    DB::beginTransaction();
             $matrix->each(function($data_input_collection, $key_chunk){
foreach ($data_input_collection as $key => $rowMatrix) {  
if ((preg_match('/^[a-zA-Z]+$/', $rowMatrix[4]) or preg_match('/^[a-zA-Z]+$/', $rowMatrix[8]))){
continue;}
                $checkOnIdEsist = DB::table('z_members')->where('id', '=', $rowMatrix[0])->exists();
                if ($checkOnIdEsist)
                { //update member
                    Member::where('id', $rowMatrix[0])->update(['first_name' => $rowMatrix[1], 'last_name' => $rowMatrix[2], 'gender' => $rowMatrix[3], 'date_of_birth' => Carbon::parse($rowMatrix[4])->format('Y-m-d') , 'phone' => $rowMatrix[5], 'email' => $rowMatrix[6], 'member_type' => $rowMatrix[7], 'member_since' => Carbon::parse($rowMatrix[8])->format('Y-m-d') , 'social_number' => $rowMatrix[9], 'address' => $rowMatrix[10], 'city' => $rowMatrix[11], 'state' => $rowMatrix[12], 'zipcode' => $rowMatrix[13], ]);

                    // additional tables update

                    Phone::where('z_members_id', $rowMatrix[0])->where('is_primary', 1)->update(['type' => 'old', 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') , 'is_primary' => 0, ]);
                    Email::where('z_members_id', $rowMatrix[0])->where('is_primary', 1)->update(['type' => 'old', 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') , 'is_primary' => 0, ]);
                    Address::where('z_members_id', $rowMatrix[0])->where('is_primary', 1)->update(['type' => 'old', 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') , 'is_primary' => 0, ]);
                    Name::where('z_members_id', $rowMatrix[0])->where('is_primary', 1)->update(['type' => 'old', 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') , 'is_primary' => 0, ]);

                    // end
                    // create additional tables

                    $phone = Phone::firstOrCreate(['z_members_id' => $rowMatrix[0]], ['phone' => $rowMatrix[5]]);
                    $phone->update(['type' => 'Primary', 'created_at' => Carbon::now()->format('Y-m-d H:i:s') ,'is_primary' => 1, 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')]);
                    $email = Email::firstOrCreate(['z_members_id' => $rowMatrix[0]], ['email' => $rowMatrix[6]]);
                    $email->update(['type' => 'Primary', 'created_at' => Carbon::now()->format('Y-m-d H:i:s') ,'is_primary' => 1, 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')]);
                    Address::firstOrCreate(['z_members_id' => $rowMatrix[0]], ['address' => $rowMatrix[10], 'city' => $rowMatrix[11], 'state' => $rowMatrix[12], 'zipcode' => $rowMatrix[13], 'type' => 'Primary', 'created_at' => Carbon::now()->format('Y-m-d H:i:s') , 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') , 'is_primary' => 1, ]);
                    $address = Address::firstOrCreate(['z_members_id' => $rowMatrix[0]], ['address' => $rowMatrix[10], 'city' => $rowMatrix[11], 'state' => $rowMatrix[12], 'zipcode' => $rowMatrix[13]]);
                    $address->update(['type' => 'Primary', 'created_at' => Carbon::now()->format('Y-m-d H:i:s') ,'is_primary' => 1, 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')]);
                    $name = Name::firstOrCreate(['z_members_id' => $rowMatrix[0]], ['first_name' => $rowMatrix[1], 'last_name' => $rowMatrix[2], 'middle_name' => '']);
                    $name->update(['type' => 'Primary', 'created_at' => Carbon::now()->format('Y-m-d H:i:s') ,'is_primary' => 1, 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')]);
                    // end

                    echo 'update';
                }
                elseif(!$checkOnIdEsist and !empty($rowMatrix[0]))
                { //create member
                    Member::create(['id' => $rowMatrix[0], 'first_name' => $rowMatrix[1], 'last_name' => $rowMatrix[2], 'gender' => $rowMatrix[3], 'date_of_birth' => Carbon::parse($rowMatrix[4])->format('Y-m-d') , 'phone' => $rowMatrix[5], 'email' => $rowMatrix[6], 'member_type' => $rowMatrix[7], 'member_since' => Carbon::parse($rowMatrix[8])->format('Y-m-d') , 'social_number' => $rowMatrix[9], 'address' => $rowMatrix[10], 'city' => $rowMatrix[11], 'state' => $rowMatrix[12], 'zipcode' => $rowMatrix[13],

                    // fields that doesn't esxist in file

                    'member_number' => $rowMatrix[0], 'drivers_license' => 0, 'alt_phone' => 0, 'ethnicity' => 0, 'profile_image_name' => 0, 'profile_image_id' => 0, 'z_locals_id' => 0, 'reference' => 0, 'created_by' => 0, 'last_position' => 0, 'points' => 0, 'monthly_dues' => 0, 'local_chapter' => 0, 'list' => 0, 'middle_name' => ' ', 'hash' => 0, 'balance' => 0, 'memo' => 0, 'paid_through' => '2099-12-06', //I can't delete this items because in db that I got this fields have property NOT NULL
                    ]);
                    Phone::create(['z_members_id' => $rowMatrix[0], 'phone' => $rowMatrix[5], 'type' => 'Primary', 'created_at' => Carbon::now()->format('Y-m-d H:i:s') , 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') , 'is_primary' => 1, ]);
                    Email::create(['z_members_id' => $rowMatrix[0], 'email' => $rowMatrix[6], 'type' => 'Primary', 'created_at' => Carbon::now()->format('Y-m-d H:i:s') , 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') , 'is_primary' => 1, ]);
                    Address::create(['z_members_id' => $rowMatrix[0], 'address' => $rowMatrix[10], 'city' => $rowMatrix[11], 'state' => $rowMatrix[12], 'zipcode' => $rowMatrix[13], 'type' => 'Primary', 'created_at' => Carbon::now()->format('Y-m-d H:i:s') , 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') , 'is_primary' => 1, ]);
                    Name::create(['z_members_id' => $rowMatrix[0], 'first_name' => $rowMatrix[1], 'last_name' => $rowMatrix[2], 'middle_name' => '', 'type' => 'Primary', 'created_at' => Carbon::now()->format('Y-m-d H:i:s') , 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') , 'is_primary' => 1, ]);
                    echo "create";

        } 
        }});
                DB::commit();
}catch(\Exception $e){
    DB::rollback();
}
}
}